#include <iostream>
#include<map>
#include <fstream>
#include<string>
#include<cstdlib>
#include"world_clock.h"
using namespace std;

map<string,int> WorldClock::timezone_;

WorldClock::WorldClock() : hour_(0),minute_(0),second_(0)
	{time_difference_=0;}

WorldClock::WorldClock(int hour,int minute,int second)
	: hour_(hour),minute_(minute),second_(second)
	{time_difference_=0;}



void WorldClock::Tick(int seconds)
{
	second_+=seconds;
	while (second_>60)
	{
		minute_++;
		second_-=60;
	}
	while(second_<0)
	{
		minute_--;
		second_+=60;
	}
	while (minute_>60)
	{
		hour_++;
		minute_-=60;
	}
	while (minute_<0)
	{
		hour_--;
		minute_+=60;
	}

	while (hour_>23)
	{
		hour_-=24;
	}
	while (hour_<0)
	{
		hour_+=24;
	}
}
bool WorldClock::SetTime(int hour, int minute, int second)
{
	if ( hour>=24||hour<0||minute>=60||minute<0||second>=60||second<0)
		return false;
	else 
	{
		hour_=hour;
		minute_=minute;
		second_=second;
	}
	return true;
}
bool WorldClock::LoadTimezoneFromFile(const string& file_path)
{
	ifstream file;
	file.open(file_path.c_str());
	if(!file.is_open())
		return false;
	while(!file.eof())
	{
		char c[20];
		int i;
		file>>c;
		file>>i;
		timezone_[c]=i;
	}
	file.close();
	return true;
}
void WorldClock::AddTimezoneInfo(const string& city,int diff)
{timezone_[city] = diff;}

void WorldClock::SaveTimezoneToFile(const std::string& file_path)
{
    ofstream fs;
    fs.open(file_path.c_str(),fstream::trunc);
    map<string,int>::iterator iter;
	for(iter=timezone_.begin();iter!=timezone_.end();iter++)
		fs<<iter->first<<" "<<iter->second<<endl;
    fs.close();
}


ostream& operator<<(ostream& os, const WorldClock& c)
{
	int h=c.hour() + c.time_difference();
	while(h>=24) h=h-24;
	os<<h<<':'<<c.minute()<<':'<<c.second();
	if(c.time_difference() != 0)
		os<<" (+"<<c.time_difference()<<')';
	return os;
}
istream& operator>>(istream& is, WorldClock& c)
{
	string tmp;
	is >> tmp;
	int hl,ml;
	for (hl=0;hl<tmp.size();hl++)
		if(tmp[hl]==':') break;
	for (ml=hl+1;ml<tmp.size();ml++)
		if(tmp[ml]==':') break;
	string h,m,s;
	for (int i=0;i<hl;i++)
		h=h+tmp[i];
	for( int i=hl+1;i<ml;i++)
		m=m+tmp[i];
	for ( int i=ml+1;i<tmp.size();i++)
		s=s+tmp[i];
	int hour,minute,second;
	hour = atoi(h.c_str());
	minute=atoi(m.c_str());
	second=atoi(s.c_str());
	if(!c.SetTime(hour,minute,second))
	{
		InvalidTimeException invalid(tmp);
		throw invalid;
	}
	else c.SetTime(hour,minute,second);
}
