#include<string>
#include<iostream>
#include"grader.h"
string SubjectPassFail::GetGrade(int score)const
{
	if (score >= pass_score_)
		return "P";
	else
		return "F";
}

string SubjectGrade::GetGrade(int score)const
{
	if (score>=cutA_)
		return "A";
	else if (score>=cutB_)
		return "B";
	else if (score>=cutC_)
		return "C";
	else if (score>=cutD_)
		return "D";
	else if (score<cutD_)
		return "F";
}
