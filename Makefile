# Makefile

all: grader world_clock

grader: grader.h grader.cc grader_main.cc
	g++ -o grader grader.cc grader_main.cc

world_clock: world_clock.h world_clock.cc world_clock_main.cc
	g++ -o world_clock world_clock.cc world_clock_main.cc

