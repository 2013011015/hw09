#include<string>
#include<iostream>
using namespace std;
class Subject {

public:

 Subject(const string& name, int credit)

     : name_(name), credit_(credit){};

 virtual ~Subject() {;}

 const string& name() const { return name_; }

 int credit() const { return credit_; }

 virtual string GetGrade(int score) const = 0;

private:

 string name_;

 int credit_;
};
class SubjectPassFail : public Subject {

public:

 SubjectPassFail(const string& name, int credit, int pass_score)
	:Subject(name,credit){pass_score_=pass_score;}

 virtual ~SubjectPassFail(){};

 virtual string GetGrade(int score) const;

private:

 int pass_score_;
};

class SubjectGrade : public Subject {

public:

 SubjectGrade(const string& name, int credit,

              int cutA, int cutB, int cutC, int cutD)
	:Subject(name,credit)
{cutA_=cutA;cutB_=cutB;cutC_=cutC;cutD_=cutD;}

 virtual ~SubjectGrade(){}

 virtual string GetGrade(int score) const;

private:

 int cutA_, cutB_, cutC_, cutD_;
};
